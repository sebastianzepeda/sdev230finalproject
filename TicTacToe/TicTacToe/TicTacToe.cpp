// TicTacToe.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;


void printTable(char Arr[][3]) {

	int row = 0;
	int column = 0;
	for (int i = 0; i < 11; i++)
	{
	

		if (column!=3)
		{
			cout << Arr[row][column] << " ";
			column++;
		}
		else
		{
			row++;
			column = 0;
			cout << endl;
		}


	}
	int x = 0;


}
void ComputerMove(char Arr[][3], char userLetter, char compLetter) {

	int row = 0;
	int column = 0;
	for (int i = 0; i < 9; i++)
	{
		if (row != 3)
		{
			if (Arr[row][column] == userLetter)
			{
				if (Arr[row+1][column] == userLetter && (row+2)<3 && column <3 && Arr[row + 2][column] != userLetter && Arr[row + 2][column] != compLetter)
				{
					Arr[row + 2][column] = compLetter;
					break;
				}
				else if (Arr[row][column+1] == userLetter && (column + 2) < 3 && row < 3 && Arr[row][column + 2] != userLetter && Arr[row][column + 2] != compLetter)
				{
					Arr[row][column+2] = compLetter;
					break;
				}
				else if (Arr[row+1][column + 1] == userLetter && (column + 2) < 3 && (row+2) < 3 && Arr[row + 2][column + 2] != userLetter && Arr[row + 2][column + 2] != compLetter)
				{
					Arr[row+2][column + 2] = compLetter;
					break;
				}
				else if ((row+1)<3 && column<3 && Arr[row + 1][column] != userLetter && Arr[row + 1][column] != compLetter)
				{
					Arr[row + 1][column] = compLetter;
					break;
				}
				else if((row + 1) < 3 && (column+1) < 3 && Arr[row + 1][column + 1] != userLetter && Arr[row + 1][column + 1] != compLetter)
				{
					Arr[row + 1][column+1] = compLetter;
					break;
				}
				else if ((row) < 3 && (column + 1) < 3 && Arr[row][column+1] != userLetter && Arr[row][column + 1] != compLetter)
				{
					Arr[row][column + 1] = compLetter;
					break;
				}



				else if ((row - 1) >= 0 && column >= 0 && Arr[row - 1][column] != userLetter && Arr[row - 1][column] != compLetter)
				{
					Arr[row - 1][column] = compLetter;
					break;

				}
				else if ((row - 1) >= 0 && (column - 1) >= 0 && Arr[row - 1][column - 1] != userLetter && Arr[row - 1][column - 1] != compLetter)
				{
					Arr[row - 1][column - 1] = compLetter;
					break;
				}
				else if ((row) >= 0 && (column - 1) >= 0 && Arr[row][column - 1] != userLetter && Arr[row][column - 1] != compLetter)
				{
					Arr[row][column - 1] = compLetter;
					break;
				}
			}
			else
			{
				row++;
			}
		}
		else
		{
			row = 0;
			column++;
		}
		
	}


}

string GameStatus(char Arr[][3], char userLetter, char compLetter)
{




	if (Arr[0][0] == userLetter)
	{
		if (Arr[0][1] == userLetter && Arr[0][2] == userLetter)
		{
			return "You Win!!";
		}
		else if(Arr[1][0] == userLetter && Arr[2][0] == userLetter)
		{
			return "You Win!!";
		}
		else if (Arr[1][1] == userLetter && Arr[2][2] == userLetter)
		{
			return "You Win!!";
		}
	}
	else if (Arr[0][1] == userLetter)
	{


		if (Arr[1][1] == userLetter && Arr[2][1] == userLetter)
		{
			return "You Win!!";
		}

	}
	else if(Arr[0][2] == userLetter)
	{
		if (Arr[1][1] == userLetter && Arr[2][0] == userLetter)
		{
			return "You Win!!";
		}
	}
	else if (Arr[1][0] == userLetter)
	{
		if (Arr[1][1] == userLetter && Arr[1][2] == userLetter)
		{
			return "You Win!!";
		}
	}
	
	int row = 0;
	int column = 0;
	for (int i = 0; i < 9; i++)
	{
		if (column != 3)
		{
			if (Arr[row][column] != userLetter || Arr[row][column] != compLetter)
			{
				return "Not Over";
			}
			column++;
		}
		else
		{
			row++;
			column = 0;
		}
	}
	return "Draw";

}


int main()
{
	int row = 0;
	int column = 0;
	char Arr[3][3];
	for (int i = 0; i < 11; i++)
	{
		if (column != 3)
		{
			Arr[row][column] ='0'+ (i+1);
			column++;
		}
		else
		{
			
			row++;
			column = 0;
			Arr[row][column] = '0'+(i + 1);
			column++;
		}
	}
	
	char userLetter;
	char computerLetter=' ';
	cout << "Choose your letter: X or O" << endl;
	cin >> userLetter;
	if (userLetter == 'X')
	{
		computerLetter = 'O';
	}
	else
	{
		computerLetter = 'X';
	}
	
	string gameStatus="Not Over";
	int userBox;
	row = 0;
	column = 0;



	while (gameStatus == "Not Over")
	{

		printTable(Arr);
		cout << endl << "Choose the number of the box you would like to place your letter" << endl;
		cin >> userBox;
		row = 0;
		column = 0;
		for (int y = 0; y < 9; y++)
		{
			if (column != 3)
			{
				if (Arr[row][column] == ('0'+userBox))
				{
					Arr[row][column] = userLetter;
					break;
				}
				column++;
			}
			else
			{

				row++;
				column = 0;
				if (Arr[row][column] == ('0' + userBox))
				{
					Arr[row][column] = userLetter;
					break;
				}
				column++;
			}
		}
		gameStatus = GameStatus(Arr, userLetter, computerLetter);
		if (gameStatus == "Draw" || gameStatus == "You Win!!" || gameStatus == "You Lost")
		{
			break;
		}


		ComputerMove(Arr, userLetter, computerLetter);


		gameStatus = GameStatus(Arr, userLetter, computerLetter);
		if (gameStatus == "Draw" || gameStatus == "You Win!!" || gameStatus == "You Lost")
		{
			break;
		}

	}
	printTable(Arr);
	cout << endl << gameStatus << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
